$(document).ready(function () {
	
	let url = window.location.href;
	let base = document.querySelector("a[href='" + url + "'")
	if(base != null){
		let hijo = base.parentNode;
		hijo.classList.add('current-page');
		hijo.parentNode.parentNode.classList.add('active');
	}
	if ($('.table').length > 0) {
		Create_Datatable('.table');
	}
	
	$('.x_panel').on('click', '.btn-destroy', function () {
		let url = $(this).attr('data-url'), mensaje = 'eliminar', mensaje2 = 'elimino';
		event.preventDefault();
		if ($(this).attr('data-original-title') != 'Eliminar') {
			mensaje = 'restaurar';
			mensaje2 = 'restauro';
		}
		swal({
			title: "¿Esta seguro de " + mensaje + " el producto?",
			type: "info",
			cancelButtonText: "Cancelar",
			confirmButtonText: "Aceptar",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function () {
			$.get(url, null, null, 'json')
				.done(function (data) {
					if (data.resp) {
						pusher(`Se ${mensaje2} el producto!`, `Se ${mensaje2} el producto`);
						setTimeout(function () {
							window.location.href = data.url;
						}, 1000);
					}
				});
		});
	});
	
	$('.x_panel').on('click', '.btn-modal', function () {
		let url = $(this).attr('data-url');
		$.get(url, null, null, 'html')
			.done(function (view) {
				$('.modal-container').html(view);
				$('#modalR').modal('show');
			})
			.fail(function (data) {
				errors(data);
			});
	});
	
	$('.modal-container').on('submit', '.form-create', function (event) {
		let url = $(this).attr('action'), dato = $(this).serialize();
		event.preventDefault();
		swal({
				title: "Desea enviar el formulario",
				confirmButtonText: "Enviar",
				cancelButtonText: "Cancelar",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function () {
				$("[id*='-error']").remove();
				$('.has-error').removeClass('has-error');
				$.post(url, dato, null, 'json')
					.done(function (data) {
						pusher('Peticion exitosa', 'Exito');
						setTimeout(function () {
							window.location.href = data.url;
						}, 1000);
					})
					.fail(function (data) {
						errors(data);
					});
			});
		
	});
	
	$('.form-login').submit(function (event) {
		let url = $(this).attr('action'), dato = $(this).serialize();
		event.preventDefault();
		swal({
				title: "Desea ingresar al sistema",
				text: "Confirmar peticion",
				confirmButtonText: "Enviar",
				cancelButtonText: "Cancelar",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function () {
				$("[id*='-error']").remove();
				$('.has-error').removeClass('has-error');
				$.post(url, dato, null, 'json')
					.done(function (data) {
						pusher('Ingreso correcto', 'Gracias por ingresar');
						setTimeout(function () {
							window.location.href = data.url;
						}, 1000);
					})
					.fail(function (data) {
						errors(data);
					});
			});
		
	});
	
	function errors(data) {
		swal({title: "Error", text: "Errores encontrados", type: "error", timer: 1000, showConfirmButton: false});
		$.each(data.responseJSON, function (i, val) {
			$('#' + i + '-error').remove();
			$('#' + i).parent().addClass('has-error').append("<span id='" + i + "-error' class='help-block help-block-error'>" + val + "</span>");
		});
	}
	
	function pusher(title, mensaje) {
		$("#easyNotify").easyNotify({
			title: `${title}`,
			options: {
				body: `${mensaje}`,
				icon: `${window.location.origin}/img/icon.png`,
				lang: 'es-Es',
				timeout: 2000,
				onClick: function () {
					window.onfocus();
				}
			}
		});
	}
	
	function Create_Datatable(clas) {
		$(`${clas}`).DataTable({
			"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, 'All']], "language": {
				"aria": {
					"sortAscending": ": Actilet para ordenar la columna de manera ascendente",
					"sortDescending": ": Actilet para ordenar la columna de manera descendente"
				},
				"infoFiltered": "(filtrado  de un total de _MAX_ registros)",
				"lengthMenu": "<span class='seperator'></span>Mostrar _MENU_ registros",
				"sProcessing": "Procesando...",
				"info": "<span class='seperator'></span>Mostrando registros del _START_ al _END_",
				"infoEmpty": "Mostrando registros del 0 al 0",
				"emptyTable": "Ningún dato disponible en esta tabla",
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'
				},
				"zeroRecords": "No se encontraron resultados"
			},
			responsive: true,
			buttons: [
				{
					extend: "copy",
					text: "Copiar",
					className: "btn-sm"
				},
				{
					extend: "csv",
					text: "CSV",
					className: "btn-sm"
				},
				{
					extend: "excel",
					text: "Excel",
					className: "btn-sm"
				},
				{
					extend: "pdfHtml5",
					text: "PDF",
					className: "btn-sm"
				},
				{
					extend: "print",
					text: "Imprimir",
					className: "btn-sm"
				},
			]
		});
	}
	
});