<!DOCTYPE html>
	<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta Http-Equiv="Cache-Control" Content="no-cache">
		<meta Http-Equiv="Pragma" Content="no-cache">
		<meta Http-Equiv="Expires" Content="0">
		<title>Gentelella Alela! | </title>
		{{ Html::style('css/app.css') }}
	</head>
	
	<body class="login">
	<div>
	<div class="login_wrapper">
		<div class="animate form login_form">
			<section class="login_content">
				{{ Form::open(['url' => '/', 'class' => 'form form-login'])  }}
					<h1>Inicio de Sesion</h1>
					<div class="form-group">
						<input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" required>
					</div>
					<div class="form-group">
						<input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required>
					</div>
					<button class="btn btn-default" type="submit">Ingresar</button>
				{{ Form::close() }}
			</section>
		</div>
	</div>
</div>
{{ Html::script('js/app.js') }}
{{ Html::script('js/script.js') }}
</body>
</html>
