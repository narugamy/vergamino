{{ Form::open(['url' => "admin/usuario/$usuario->id", 'method' => 'put', 'class' => 'form form-horizontal form-label-left form-create']) }}
<span class="section">Personal Info</span>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Nombres</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input id="nombres" class="form-control" name="nombres" placeholder="Nombres" required type="text"  value="{{ $usuario->nombres }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellidos">Apellidos</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input id="apellidos" class="form-control" name="apellidos" placeholder="Apellidos" required type="text" value="{{ $usuario->apellidos }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Direccion</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input type="text" id="direccion" name="direccion" class="form-control col-md-7 col-xs-12" required placeholder="Descripcion" value="{{ $usuario->direccion }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dni">DNI</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input type="text" id="dni" name="dni" class="form-control col-md-7 col-xs-12" required placeholder="Descripcion" data-rule-minlength="8" data-rule-maxlength="8" value="{{ $usuario->dni }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Telefono</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input type="text" id="telefono" name="telefono" class="form-control col-md-7 col-xs-12" required placeholder="Descripcion" data-rule-minlength="7" data-rule-maxlength="9" value="{{ $usuario->telefono }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuario">Usuario</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input id="usuario" class="form-control" name="usuario" placeholder="Usuario" required type="text" value="{{ $usuario->usuario }}">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Contraseña</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input id="password" class="form-control" name="password" placeholder="Contraseña" type="password">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol_id">Rol</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		{{ Form::select('rol_id', $roles, $usuario->rol_id, ['class' => 'form-control col-md-7 col-xs-12', 'required' => '', 'id' => 'rol_id', 'placeholder' => 'Seleccionar una opcion']) }}
	</div>
</div>
<div class="clearfix"></div>
<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-5">
		<button type="submit" class="btn btn-success">Actualizar</button>
	</div>
</div>
{{ Form::close() }}
{{ Html::script('js/form-validation-md.js') }}