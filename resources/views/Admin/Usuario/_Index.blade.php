<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<a data-url="{{ url('admin/usuario/create') }}" class="btn btn-success pull-right btn-modal">Registrar
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<thead>
						<tr>
							<th class="all">N°</th>
							<th class="min-phone">Nombres</th>
							<th class="min-phone">Apellidos</th>
							<th class="min-phone">Dni</th>
							<th class="min-phone">Usuario</th>
							<th class="all">Accion</th>
						</tr>
						</thead>
						<tbody>
						@foreach($usuarios as $usuario)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $usuario->nombres }}</td>
								<td>{{ $usuario->apellidos }}</td>
								<td>{{ $usuario->dni }}</td>
								<td>{{ $usuario->usuario }}</td>
								<td>
									@if($usuario->deleted_at != null)
										<a data-url='{{ url("admin/usuario/$usuario->id/destroy") }}' class="btn btn-icon-toggle btn-destroy" data-original-title="Recuperar"> <i class="fa fa-recycle"></i></a>
									@else
										<a data-url='{{ url("admin/usuario/$usuario->id/destroy") }}' class="btn btn-icon-toggle btn-destroy" data-original-title="Eliminar"> <i class="fa fa-trash-o"></i></a>
									@endif
										<a data-url='{{ url("admin/usuario/$usuario->id") }}' class="btn btn-icon-toggle btn-modal" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i></a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalR" tabindex="-1" role="dialog" aria-labelledby="modalRLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body modal-container">
			
			</div>
		</div>
	</div>
</div>