<?php

namespace App\Http\Requests\Login;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest {
	
	public function authorize() {
		return true;
	}
	
	public function rules() {
		return [
			'usuario' => 'required|min:4|max:45|exists:usuario',
			'password' => 'required|min:4|max:15',
		];
	}
	
	public function messages(){
		return [
			'usuario.required'=>'requerido',
			'usuario.max'=>'min. :max caracteres',
			'usuario.min'=>'max. :min caracteres',
			'usuario.exists'=>'El usuario no existe',
			'password.required'=>'requerido',
			'password.max'=>'min. :max caracteres',
			'password.min'=>'max. :min caracteres'
		];
	}
	
}
