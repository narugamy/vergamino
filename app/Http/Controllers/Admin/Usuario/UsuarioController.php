<?php

namespace App\Http\Controllers\Admin\Usuario;

use App\Model\Rol;
use App\Model\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller {
	
	public function index() {
		$usuarios = Usuario::withTrashed()->get();
		return view('Admin.Usuario.Index')->with('usuarios', $usuarios);
	}
	
	public function create() {
		$roles = Rol::orderBy('nombre')->pluck('nombre', 'id');
		return view('Admin.Usuario._Create')->with('roles', $roles);
	}
	
	public function store(Request $request) {
		try {
			DB::beginTransaction();
			$usuario = new Usuario($request->all());
			$usuario->password = bcrypt($usuario->password);
			$usuario->save();
			DB::commit();
			return response()->json(['resp' => 'true', 'url' => url('admin/usuario')]);
		} catch (Exception $e) {
			DB::rollBack();
			return response()->json(['resp' => 'false', 'url' => url('admin/usuario')], 422);
		}
	}
	
	public function show($id) {
		$roles = Rol::orderBy('nombre')->pluck('nombre', 'id');
		$usuario = Usuario::withTrashed()->find($id);
		return view('Admin.Usuario._Edit')->with('usuario', $usuario)->with('roles', $roles);
	}
	
	public function update(Request $request, $id) {
		try {
			DB::beginTransaction();
			$usuario = Usuario::withTrashed()->find($id);
			$usuario->rol_id = $request->rol_id;
			$usuario->nombres = $request->nombres;
			$usuario->apellidos = $request->apellidos;
			$usuario->direccion = $request->direccion;
			$usuario->dni = $request->dni;
			$usuario->telefono = $request->telefono;
			$usuario->usuario = $request->usuario;
			$usuario->password = ($request->password)? bcrypt($request->password): $usuario->password;
			$usuario->save();
			DB::commit();
			return response()->json(['resp' => 'true', 'url' => url('admin/usuario')]);
		} catch (Exception $e) {
			DB::rollBack();
			return response()->json(['resp' => 'false', 'url' => url('admin/usuario')], 422);
		}
	}
	
	public function destroy($id) {
		try {
			DB::beginTransaction();
			$usuario = Usuario::withTrashed()->find($id);
			if ($usuario->deleted_at != null):
				$usuario->restore();
			else:
				$usuario->delete();
			endif;
			DB::commit();
			return response()->json(['resp' => 'true', 'url' => url('admin/usuario')]);
		} catch (Exception $e) {
			DB::rollBack();
			return response()->json(['resp' => 'false', 'url' => url('admin/usuario')], 422);
		}
	}
	
}
