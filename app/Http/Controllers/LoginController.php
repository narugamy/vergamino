<?php

namespace App\Http\Controllers;

use App\Http\Requests\Login\LoginRequest;
use App\Model\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller {
	
	function index(){
		return view('Login');
	}
	
	public function login(LoginRequest $request) {
		$usuario = Usuario::where('usuario', $request->usuario)->with('rol')->first();
		if (empty($usuario)):
			return response()->json(['resp' => false, 'url' => url('/'), 'usuario' => 'usuario erroneo'], 422);
		else:
			if (Hash::check($request->password, $usuario->password)):
				if ($usuario->admin()):
					Auth::login($usuario);
					return response()->json(['resp' => true, 'url' => url('admin'), 'mensaje' => 'Ingreso correcto']);
				elseif ($usuario->usuario()):
					Auth::login($usuario);
					return response()->json(['resp' => true, 'url' => url('admin'), 'mensaje' => 'Ingreso correcto']);
				else:
					return response()->json(['resp' => false, 'url' => url('/')], 422);
				endif;
			else:
				return response()->json(['resp' => false, 'url' => url('/'), 'password' => 'contraseña erroneo'], 422);
			endif;
		endif;
	}
	
	
	public function destroy() {
		Session::flush();
		Auth::logout();
		return redirect::to('/');
	}
	
}
