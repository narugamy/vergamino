<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incidencia extends Model {
	
	use SoftDeletes;
	
	protected $table = "incidencia";
	
	protected $fillable = ['usuario_id', 'fecha'];
	
	public function usuarios(){
		return $this->hasMany('App\Model\Usuario');
	}
	
	
}
