<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Usuario extends Authenticatable {
	
	use SoftDeletes, Notifiable;
	
	protected $table = 'usuario';
	
	protected $fillable = ['rol_id', 'nombres', 'apellidos', 'direccion', 'dni', 'telefono', 'usuario', 'password'];
	
	protected $hidden = ['password', 'remember_token'];
	
	public function rol(){
		return $this->belongsTo('App\Model\Rol');
	}
	
	public function admin(){
		return (int)$this->rol_id === 1;
	}
	
	public function usuario(){
		return (int)$this->rol_id === 2;
	}
	
}
