<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model {
	
	use SoftDeletes;
	
	protected $table = "rol";
	
	protected $fillable = ['nombre'];
	
	public function usuarios(){
		return $this->hasMany('App\Model\Usuario');
	}
	
}
