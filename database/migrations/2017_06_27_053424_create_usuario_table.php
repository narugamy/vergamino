<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration {
	
	public function up() {
		Schema::create('usuario', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('rol_id')->unsigned();
			$table->foreign('rol_id')->references('id')->on('rol');
			$table->string('nombre');
			$table->string('apellidos');
			$table->string('direccion');
			$table->char('dni', 8);
			$table->char('telefono', 9);
			$table->string('usuario')->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});
	}
	
	public function down() {
		Schema::dropIfExists('usuario');
	}
	
}
