<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRolTable extends Migration {
	
	public function up() {
		Schema::create('rol', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre',191);
			$table->timestamps();
			$table->softDeletes();
		});
	}
	
	public function down() {
		Schema::dropIfExists('rol');
	}
	
}
