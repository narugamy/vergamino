<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciaTable extends Migration {
	
	public function up() {
		Schema::create('incidencia', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('usuario_id')->unsigned();
			$table->foreign('usuario_id')->references('id')->on('usuario');
			$table->date('fecha');
			$table->timestamps();
			$table->softDeletes();
		});
	}
	
	public function down() {
		Schema::dropIfExists('incidencia');
	}
	
}
